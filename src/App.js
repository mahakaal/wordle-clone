import React, { useEffect, useState } from 'react'
import {
	useQuery
} from 'react-query'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Wordle from './components/Wordle'
import {
	collection,
	getDocs
} from 'firebase/firestore/lite'
import { db } from './firebase-config'

const fetchSolutionData = async () => {
	const solutionSnap = await getDocs(collection(db, 'solutions'));
	const solutionList = solutionSnap.docs.map(doc => doc.data());

	return solutionList;
}

function App() {
	const [solution, setSolution] = useState(null);
	const {data, status} = useQuery('solutions', fetchSolutionData);

	useEffect(() => {
		const randomSolution = data ? data[Math.floor(Math.random()*data?.length)] : '';

		setSolution(randomSolution.word);
	}, [data]);

	return (
		<div className="App">
			<h1>Wordel (Lingo)</h1>
			{status === 'loading' && <div>Loading .....</div>}
			<BrowserRouter>
				<Routes>
					{status === 'success' && <Route path='/' element={ <Wordle solution={solution} />} />}
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;
