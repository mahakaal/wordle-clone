// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore/lite'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBKp7gE8RNWRIzzoQ-uSnP-kwUwk79i5wk",
  authDomain: "wordle-clone-7d321.firebaseapp.com",
  projectId: "wordle-clone-7d321",
  storageBucket: "wordle-clone-7d321.appspot.com",
  messagingSenderId: "216949882709",
  appId: "1:216949882709:web:dc9656582eadd8b43539dc"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { app, db }