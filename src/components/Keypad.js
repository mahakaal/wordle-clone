import React, { useEffect, useState } from 'react'
import {
    useQuery
} from 'react-query'
import {
	collection,
	getDocs
} from 'firebase/firestore/lite'
import { db } from '../firebase-config'

const fetchLetters = async () => {
    const lettersSnap = await getDocs(collection(db, 'letters'));
    const lettersList = lettersSnap.docs.map(doc => doc.data().key);
    const result = lettersList.sort().map(letter => {
        return {
            key: letter
        }
    })
    
    return result;
}

function Keypad({ usedKeys }) {
    const [letters, setLetters] = useState(null);
    const { data, status } = useQuery('letters', fetchLetters);

    useEffect(() => {
        setLetters(data);
    }, [data])
    return (
        <div className='keypad'>
            { status === 'success' && letters?.map((letter => {
                return (
                    <div key={letter.key} className={usedKeys[letter.key]}>
                        {letter.key}
                    </div>
                )
            }))}
        </div>
    )
}

export default Keypad