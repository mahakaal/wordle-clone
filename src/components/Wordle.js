import React, { useEffect, useState } from 'react'
import useWordle from '../hooks/useWordle'
import Grid from './Grid';
import Keypad from './Keypad';
import Modal from './Modal';

function Wordle({ solution }) {
    const { currentGuess, handleKeyUp, guesses, isCorrect, turn, usedKeys, resetGame } = useWordle(solution);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        window.addEventListener('keyup', handleKeyUp)

        if (isCorrect) {
            setTimeout(() => setShowModal(true), 2000)
            window.removeEventListener('keyup', handleKeyUp)
        }

        if (turn > 5) {
            setTimeout(() => setShowModal(true), 2000)
            window.removeEventListener('keyup', handleKeyUp)
        }

        return () => window.removeEventListener('keyup', handleKeyUp)
    }, [handleKeyUp, isCorrect, turn, resetGame])
    
    return (
        <>
            <div className='wordle-grid'>
                <Grid 
                    currentGuess={currentGuess} 
                    guesses={guesses}
                    turn={turn}
                />
                <Keypad usedKeys={usedKeys} />
            </div>
            {
                showModal && 
                <Modal 
                    isCorrect={isCorrect}
                    turn={turn}
                    solution={solution}
                    setShowModal={setShowModal}
                    resetGame={resetGame}
                />
            }
        </>
    )
}

export default Wordle