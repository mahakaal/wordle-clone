import React from 'react'

function Modal({ isCorrect, solution, turn, setShowModal, resetGame }) {
    const resetGameHandler = () => {
        setTimeout(() => {
            setShowModal(false);
            resetGame();
            window.location.reload();
        }, 1000)
    }

    return (
        <div className="modal">
            {isCorrect && (
                <div>
                    <h1 className='correct'>You Win 😍😍😍😍😍</h1>
                    <p className="solution">{solution}</p>
                    <p>You found the solution in {turn} guesses :)</p>
                    <button className='close-button' onClick={resetGameHandler}>X</button>
                </div>
            )}
            {!isCorrect && (
                <div>
                    <h1 className='wrong'>Nevermind 😞😞😞😞😞</h1>
                    <p className="solution">{solution}</p>
                    <p>Better luck next time :)</p>
                    <button className='close-button' onClick={resetGameHandler}>X</button>
                </div>
            )}
        </div>
    )
}

export default Modal