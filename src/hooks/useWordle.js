import { useState } from "react"

const useWordle = (solution) => {

    const [turn, setTurn] = useState(0);
    const [currentGuess, setCurrentGuess] = useState('');
    const [guesses, setGuesses] = useState([...Array(6)]);
    const [history, setHistory] = useState([]);
    const [isCorrect, setIsCorrect] = useState(false);
    const [usedKeys, setUsedKeys] = useState({});

    const resetGame = () => {
        setTurn(0);
        setCurrentGuess('');
        setGuesses([...Array(6)]);
        setHistory([]);
        setIsCorrect(false);
        setUsedKeys({});
    }
    
    const formatGuess = () => {
        let solutionArray = [...solution];
        let formattedGuess = [...currentGuess].map((letter) => {
            return {
                key: letter,
                color: 'grey'
            }
        })
        
        // find any green letters
        formattedGuess.forEach((letter, i) => {
            if(solutionArray[i] === letter.key) {
                formattedGuess[i].color = 'green';
                solutionArray[i] = null
            }
        })

        // find any yellow color
        formattedGuess.forEach((letter, i) => {
            if(solutionArray.includes(letter.key) && letter.color !== 'green') {
                formattedGuess[i].color = 'yellow';
                solutionArray[solutionArray.indexOf(letter.key)] = null
            }
        })
        
        return formattedGuess;
    }

    const addNewGuess = (formattedGuess) => {
        if(currentGuess === solution) {
            setIsCorrect(true);
        }

        setGuesses((prevGuesses) => {
            let newGuesses = [...prevGuesses];
            newGuesses[turn] = formattedGuess;
            return newGuesses;
        })

        setHistory((prevValue) => {
            return [...prevValue, currentGuess]
        })

        setTurn((prevTurn) => {
            return prevTurn + 1;
        })

        setUsedKeys((prevUsedKeys) => {
            formattedGuess.forEach(letter => {
              const currentColor = prevUsedKeys[letter.key]
      
              if (letter.color === 'green') {
                prevUsedKeys[letter.key] = 'green'
                return
              }
              if (letter.color === 'yellow' && currentColor !== 'green') {
                prevUsedKeys[letter.key] = 'yellow'
                return
              }
              if (letter.color === 'grey' && currentColor !== ('green' || 'yellow')) {
                prevUsedKeys[letter.key] = 'grey'
                return
              }
            })
      
            return prevUsedKeys;

        })

        setCurrentGuess('')
    }

    const handleKeyUp = ({ key }) => {
        // For submiting the word guess
        if(key === 'Enter') {
            // Only add guess if thurn is less than 5
            if(turn > 5) {
                return;
            }
            // Do not allow duplicate words
            if(history.includes(currentGuess)) {
                setCurrentGuess('');
                return;
            }
            // Check word is 5 char long
            if(currentGuess.length !== 5) {
                return;
            }
            
            addNewGuess(formatGuess());
        }
        // For deleting the word guess
        if(key === 'Backspace') {
            setCurrentGuess((prev) => {
                return prev.slice(0, -1)
            })
            return
        }
        // Check if the key press is alphabet
        if(/^[A-Za-z]$/i.test(key)) {
            if(currentGuess.length < 5) {
                setCurrentGuess((prev) => {
                    return prev + key
                })
            }
        }
    }

    return {
        turn,
        currentGuess,
        guesses,
        isCorrect,
        handleKeyUp,
        usedKeys,
        resetGame
    }
}

export default useWordle